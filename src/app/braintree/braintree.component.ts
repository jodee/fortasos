import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgxBraintreeService} from '../ngx-braintree-service.service';


declare var braintree: any;


@Component({
  selector: 'app-braintree',
  templateUrl: './braintree.component.html',
  styleUrls: ['./braintree.component.css']
})
export class BraintreeComponent implements OnInit {

  @Input() clientTokenURL: string;
  @Input() createPurchaseURL: string;
  @Output() paymentStatus: EventEmitter<any> = new EventEmitter<any>();
  clientToken: string;
  showDropinUI = true;
  clientTokenNotReceived = false;
  interval: any;
  instance: any;

// Optional inputs
  @Input() buttonText = 'Buy';


  constructor(private service: NgxBraintreeService) {
  }

  ngOnInit() {

    this.service
      .getClientToken(this.clientTokenURL)
      .subscribe((clientToken: string) => {
        this.clientToken = clientToken;
        this.clientTokenNotReceived = false;
        this.interval = setInterval(() => {
          this.createDropin();
        }, 0);
      }, (error) => {
        this.clientTokenNotReceived = true;
        console.log(`Client token not received. Please make sure your braintree server api is configured properly, running and accessible.`);
      });

  }

  createDropin() {
    if (typeof braintree !== 'undefined') {
      braintree.dropin.create({
        authorization: this.clientToken,
        container: '#dropin-container',
        // vaultManager: true,
        paypal: {
          flow: 'checkout',
          amount: 45.00,
          currency: 'EUR'
        }
      }, (createErr, instance) => {
        this.instance = instance;
      });
      clearInterval(this.interval);
    }
  }

  pay(): void {
    if (this.instance) {
      this.instance.requestPaymentMethod((err, payload) => {
        this.showDropinUI = false;
        this.service
          .createPurchase(this.createPurchaseURL, payload.nonce)
          .subscribe((status: any) => {
            this.paymentStatus.emit(status);
          });
      });
    }
  }
}
