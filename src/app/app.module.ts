import { BrowserModule } from '@angular/platform-browser';
import {Component, EventEmitter, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { BraintreeComponent } from './braintree/braintree.component';
import {NgxBraintreeService} from './ngx-braintree-service.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    BraintreeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [NgxBraintreeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
