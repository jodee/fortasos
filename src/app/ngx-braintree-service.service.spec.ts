import { TestBed } from '@angular/core/testing';

import { NgxBraintreeServiceService } from './ngx-braintree-service.service';

describe('NgxBraintreeServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgxBraintreeServiceService = TestBed.get(NgxBraintreeServiceService);
    expect(service).toBeTruthy();
  });
});
