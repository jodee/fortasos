import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class NgxBraintreeService {

  constructor(private http: HttpClient) { }

  getClientToken(clientTokenURL: string): Observable<string> {
    return this.http
      .get(clientTokenURL, { responseType: 'json' })
      .pipe(map((response: any) => {
        console.log(response.token);
        return response.token;
      }));
  }

  createPurchase(createPurchaseURL: string, nonce: string): Observable<any> {
    const headers = new HttpHeaders();
    console.log(nonce);
    const formaData = new FormData();
    formaData.append('nonce' , nonce);
    formaData.append('planId' , 'ff80808169f2b77d0169f2ba063b0000');
    return this.http
      .post(createPurchaseURL, formaData, { 'headers': headers })
      .pipe(map((response: any) => {
        return response;
      }));
  }

}
